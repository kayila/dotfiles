function sand() {
	cd ~/sandbox/$@
}

function  _sandcomplete() {
	local _opts="$(ls ~/sandbox)"
	local _cur="$2"

	COMPREPLY=( $(compgen -W "${_opts}" -- ${_cur}) )
}

#complete -o nospace -F _sandcomplete sand
complete -o nospace -C _sand_completion sand

# Count down number of seconds
countdown() {
    start="$(( $(date '+%s') + $1))"
    while [ $start -ge $(date +%s) ]; do
        time="$(( $start - $(date +%s) ))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
}

stopwatch() {
    start=$(date +%s)
    while true; do
        time="$(( $(date +%s) - $start))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
}



###################################################
# The following docker commanders were yoinked from
# https://blog.ropnop.com/docker-for-pentesters/
# Dockerfiles can be found at
# https://github.com/ropnop/dockerfiles
###################################################

# Run /bin/bash in provided container
function dockershell() {
    docker run --rm -i -t --entrypoint=/bin/bash "$@"
}

# Run /bin/sh in provided container
function dockershellsh() {
    docker run --rm -i -t --entrypoint=/bin/sh "$@"
}

# Run /bin/bash in provided container, mounting the current directory
function dockershellhere() {
    dirname=${PWD##*/}
    docker run --rm -it --entrypoint=/bin/bash -v `pwd`:/${dirname} -w /${dirname} "$@"
}

# Run /bin/sh in provided container, mounting the current directory
function dockershellshhere() {
    docker run --rm -it --entrypoint=/bin/sh -v `pwd`:/${dirname} -w /${dirname} "$@"
}

# Run a windows shell, mounting the current directory
# TODO Set up https://github.com/StefanScherer/windows-docker-machine
function dockerwindowshellhere() {
    dirname=${PWD##*/}
    docker -c 2019-box run --rm -it -v "C:${PWD}:C:/source" -w "C:/source" "$@"
}

# Run impacket
impacket() {
    docker run --rm -it rflathers/impacket "$@"
}

# Create a samba server sharing the current folder
smbservehere() {
    local sharename
    [[ -z $1 ]] && sharename="SHARE" || sharename=$1
    docker run --rm -it -p 445:445 -v "${PWD}:/tmp/serve" rflathers/impacket smbserver.py -smb2support $sharename /tmp/serve
}

# Run nginx with both http & https using self-signed cert in current folder
nginxhere() {
    docker run --rm -it -p 80:80 -p 443:443 -v "${PWD}:/srv/data" rflathers/nginxserve
}

# Create a webdav server, sharing current folder
webdavhere() {
    docker run --rm -it -p 80:80 -v "${PWD}:/srv/data/share" rflathers/webdav
}

# Run metasploit
metasploit() {
    docker run --rm -it -v "${HOME}/.msf4:/home/msf/.msf4" metasploitframework/metasploit-framework ./msfconsole "$@"
}

# Run metasploit, opening ports 8443-8500 for servers
metasploitports() {
    docker run --rm -it -v "${HOME}/.msf4:/home/msf/.msf4" -p 8443-8500:8443-8500 metasploitframework/metasploit-framework ./msfconsole "$@"
}

# Run msfvenom in the current dir
msfvenomhere() {
    docker run --rm -it -v "${HOME}/.msf4:/home/msf/.msf4" -v "${PWD}:/data" metasploitframework/metasploit-framework ./msfvenom "$@"
}

# HTTP Request dumper
reqdump() {
    docker run --rm -it -p 80:3000 rflathers/reqdump
}

# Store files posted to container in current directory
postfiledumphere() {
    docker run --rm -it -p80:3000 -v "${PWD}:/data" rflathers/postfiledump
}

###################################################
# End yoinked docker commands
###################################################


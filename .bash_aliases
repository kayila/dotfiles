alias strstrip='python -c "from __future__ import print_function; import sys; print(sys.stdin.read().strip(), end=\"\")"'
alias copy='xclip -selection clipboard -i'
alias paste='xclip -selection clipboard -o'
alias scopy='strstrip | copy'

# Generate a random uuid
alias uuid='python3 -c "import uuid; print(uuid.uuid4())"'
# Generate a fake ip
alias mkip="python3 -c 'import random; import socket; import struct; print(socket.inet_ntoa(struct.pack(\">I\", random.randint(1, 0xffffffff))))'"

alias intense-yipper="rdesktop -p - -g 1920x1048 -r disk:shared-folder=/home/kayila/winshare intense-yipper.fox"

# Fucking migraines
alias migraine="redshift -o -O 5000 -b 0.5 -P"
alias unmigraine="redshift -x"

# Make which search aliases and functions too
#alias which='(alias; declare -f) | /usr/bin/which --tty-only --read-alias --read-functions --show-tilde --show-dot'
alias which='command -v'

# Run FlashGBX
# https://www.gbxcart.com/
# Install via https://github.com/lesserkuma/FlashGBX#installing-and-running
alias flashgbx="python3 -m FlashGBX"

# Find temp files
alias findtmp="find -name '*~' -print"

alias unix2dos="sed -i '/\r/! s/$/\r/'"
alias dos2unix="sed -i 's/\r$//'"

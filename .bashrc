# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
      # We have color support; assume it's compliant with Ecma-48
      # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
      # a case would tend to support setf rather than setaf.)
      color_prompt=yes
    else
      color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

##########################################################################
# Helper functions
##########################################################################

# Prepend a folder to the path
function prefixToPath {
  # Check if it's already in the path
  echo "${PATH}" | grep -q "${1}"
  rc="$?"
  if [ -d "${1}" ] && [ "$rc" != "0" ]; then
    PATH="${1}:${PATH}"
  fi
}

# Add a folder to the path
# Checks first if a folder exists then if it's already in the path. Only
# adds the folder to the path if it's not already there.
function pathadd {
  if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
    PATH="${PATH:+"$PATH:"}$1" # Add to end of path
    #PATH="$1${PATH:+":$PATH"}" # Add to start of path
  fi
}

# Attempts to 'safely' source a file by verifying that the owner is set to
# the current user, and the file is only user writable
# TODO NOT FINISHED YET
function safeLoad {
  file="${1}"
  [ -O "${file}" ]
  stat "${file}"
}

# Add ~/bin to path
prefixToPath ~/bin

# Setup tfenv for terraform
# https://github.com/tfutils/tfenv
# git clone https://github.com/tfutils/tfenv.git ~/.tfenv
prefixToPath ~/.tfenv/bin

# Set up Rust & rustup, if it exists
# https://rustup.rs
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
prefixToPath ~/.cargo/bin

# Set up nvm for node.js
# https://github.com/nvm-sh/nvm
# git clone https://github.com/nvm-sh/nvm.git ~/.nvm
# git checkout $(git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1))
if [ -d ~/.nvm ]; then
  . ~/.nvm/nvm.sh
fi


# Configure virtualenvwrapper (for python)
# pip install virtualenvwrapper
if [ -f /usr/bin/virtualenvwrapper.sh ]; then
  export WORKON_HOME=~/.pyenvs
  if [ ! -d $WORKON_HOME ]; then
    mkdir -p $WORKON_HOME
  fi
  . /usr/bin/virtualenvwrapper.sh
fi

# Configure rbenv for Ruby
# https://github.com/rbenv/rbenv
# git clone https://github.com/rbenv/rbenv.git ~/.rbenv
# cd ~/.rbenv && src/configure && make -C src # It is ok if this fails
p=~/.rbenv/bin
echo "${PATH}" | grep -q $p
rc="$?"
if [ -d $p ] && [ "$rc" != "0" ]; then
  export PATH="$p:$PATH"
  eval "$(rbenv init -)"
fi

# Source .bash_aliases, if it exists
if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# Source .bash_functions, if it exists
if [ -f ~/.bash_functions ]; then
  . ~/.bash_functions
fi

# Source .bash_exports, if it exists
if [ -f ~/.bash_exports ]; then
  . ~/.bash_exports
fi

# Source host specific bashrc
if [ -f ~/.bash_local ]; then
  . ~/.bash_local
fi

# For fucks sake, give me a reasonable fucking editor
export EDITOR=nvim

# Let's get pishock completions
[[ -s /home/kayila/.bash_completions/pishock.sh ]] && source /home/kayila/.bash_completions/pishock.sh

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/kayila/.sdkman"
[[ -s "/home/kayila/.sdkman/bin/sdkman-init.sh" ]] && source "/home/kayila/.sdkman/bin/sdkman-init.sh"



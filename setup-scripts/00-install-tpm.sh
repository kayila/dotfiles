#!/usr/bin/env bash

INSTALL=${INSTALL:-1}

while getopts 'iu' OPTION; do
	case "${OPTION}" in
		i)
			INSTALL=1
			;;
		u)
			INSTALL=0
			;;
		?)
			echo "Usage: $(basename ${0}) [-i] [-u]"
	esac
done
shift "$(($OPTIND -1))"

if [ "${INSTALL}" -eq "1" ]; then
	echo "Cloning tmux plugin manager to ~/.tmux/plugins/tpm"
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
else
	echo "Removing tmux plugin manager folder ~/.tmux/plugins/tpm"
	rm -rf ~/.tmux/plugins/tpm
fi
